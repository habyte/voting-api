const Server = require('./src/server')
const Database = require('./src/database')

const server = Server.start()
const database = Database.connect()

module.exports = { server, database }

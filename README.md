# Voting API

## Rodando o projeto
- Instalar Node v12.16.1+
- Instalar Mongo v4.2.5
- Inicializar o mongo
    - Linux: `$ sudo service mongod start`
- Instalar as dependências do projeto
    - `$ npm install`
- Inicializar o servidor
    - `$ npm start`

## Casos de uso
- Cadastrar uma nova pauta
    - POST `/themes`
    - Body (exemplo): `{ "question": "Devemos aumentar a taxa de juros?" }`
    - Obs: é possível especificar um array de strings `allowedOptions`, caso "Sim" e "Não" não sejam suficientes.
- Abrir uma sessão de votação em uma pauta (a sessão de votação deve ficar aberta por um tempo determinado na chamada de abertura ou 1 minuto por default)
    - POST `/sessions`
    - Body (exemplo): `{ "themeId": "{{_id retornado pelo /themes}}", startTime: "2020-05-17T20:00:00Z", endTime: "2020-05-17T22:00:00Z" }`
    - Obs: `startTime` e `endTime` são opcionais.
- Receber votos dos associados em pautas (os votos são apenas 'Sim'/'Não'. Cada associado é identificado por um id único e pode votar apenas uma vez por pauta)
    - POST `/votes`
    - Body (exemplo): `{ "sessionId": "{{_id retornado pelo /sessions}}", "associateId": "01234567890", "content": "ok" }`
- Contabilizar os votos e dar o resultado da votação na pauta
    - GET `/sessions/{{_id retornado pelo /sessions}}/results`
    - Resposta (exemplo):

```json
{
  "question": "Devemos aumentar a taxa de juros?",
  "answers": {
    "mostFrequent": "Sim",
    "count": {
      "Sim": 51,
      "Não": 49
    }
  }
}
```

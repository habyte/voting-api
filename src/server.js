const express = require('express')

const config = require('../config')
const ThemesController = require('../src/controllers/themes.controller')
const SessionsController = require('../src/controllers/sessions.controller')
const VotesController = require('../src/controllers/votes.controller')

class Server {
    static start() {
        const app = express()

        app.use(express.json())

        app.post('/themes', ThemesController.create)
        app.post('/sessions', SessionsController.create)
        app.post('/votes', VotesController.create)

        app.get('/sessions/:id/results', SessionsController.showResults)

        app.listen(config.server.port, () => {
            console.log(`Server running on port ${config.server.port}`);
        })

        return app
    }
}

module.exports = Server

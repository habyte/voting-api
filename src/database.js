const mongoose = require('mongoose')

const config = require('../config')

class Database {
    static connect() {
        mongoose.connect(config.database.address, config.database.options)

        return mongoose
    }
}

module.exports = Database

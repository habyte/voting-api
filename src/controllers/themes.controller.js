const Theme = require('../models/theme')

class ThemesController {
    static async create(req, res) {
        try {
            const { question, allowedAnswers } = req.body

            const theme = await Theme.create({ question, allowedAnswers })

            res.status(201).send(theme)
        } catch (e) {
            if (e.name == "ValidationError") {
                res.status(422)
            } else {
                res.status(500)
            }

            res.send(e.message)
        }
    }
}

module.exports = ThemesController

const Theme = require('../models/theme')
const Session = require('../models/session')
const Vote = require('../models/vote')
const ResultsView = require('../views/results.view')

class SessionsController {
    static async create(req, res) {
        try {
            const { themeId, startTime, endTime } = req.body

            const session = await Session.create({ themeId, startTime, endTime })

            res.status(201).send(session)
        } catch (e) {
            if (e.name == "ValidationError") {
                res.status(422)
            } else {
                res.status(500)
            }

            res.send(e.message)
        }
    }

    static async showResults(req, res) {
        try {
            const sessionId = req.params.id

            const session = await Session.findById(sessionId)
            const theme = await Theme.findById(session.themeId)
            const votes = await Vote.find({ sessionId })

            const answers = votes.reduce(voteCounter, { mostFrequent: null, count: {} })

            res.send(ResultsView.render(theme, answers))
        } catch (e) {
            console.log(e.message)

            res.sendStatus(404)
        }
    }
}

const voteCounter = (acc, vote) => {
    const answer = vote.content

    if (!acc.count[answer]) acc.count[answer] = 0

    acc.count[answer] += 1

    if (!acc.mostFrequent || acc.count[answer] > acc.mostFrequent) {
        acc.mostFrequent = answer
    }

    return acc
}

module.exports = SessionsController

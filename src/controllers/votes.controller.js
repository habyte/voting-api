const Vote = require('../models/vote')
const Session = require('../models/session')

class VotesController {
    static async create(req, res) {
        try {
            const { sessionId, associateId, content } = req.body
            
            const { startTime, endTime } = await Session.findById(sessionId)
            const currentTime = Date.now()

            if (currentTime < startTime || currentTime > endTime) {
                throw new Error("VotingSessionExpired")
            }

            await Vote.create({ sessionId, associateId, content })

            res.sendStatus(201)
        } catch (e) {
            if (e.message == "VotingSessionExpired") {
                res.status(400)
            } else if (e.name == "ValidationError") {
                res.status(422)
            } else {
                res.status(500)
            }

            res.send(e.message)
        }
    }
}

module.exports = VotesController

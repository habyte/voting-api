const { Schema, model, ObjectId } = require('mongoose')

const sessionSchema = new Schema({
    themeId: {
        type: ObjectId,
        ref: 'Theme',
        required: true,
    },
    startTime: {
        type: Date,
        required: true,
        default: new Date(),
    },
    endTime: {
        type: Date,
        required: true,
        default: new Date(new Date().getTime() + 60000),
    },
})

module.exports = model('Session', sessionSchema)

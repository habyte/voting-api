const { Schema, model, ObjectId } = require('mongoose')

const voteSchema = new Schema({
    sessionId: {
        type: ObjectId,
        ref: 'Session',
        required: true,
        index: true,
    },
    associateId: {
        type: String,
        required: true,
        minlength: 11,
        maxlength: 11,
    },
    content: {
        type: String,
        required: true,
    },
})

module.exports = model('Vote', voteSchema)

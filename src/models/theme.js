const { Schema, model } = require('mongoose')

const themeSchema = new Schema({
    question: {
        type: String,
        required: true,
        unique: true,
    },
    allowedAnswers: {
        type: [String],
        default: ['Sim', 'Não'],
    },
})

module.exports = model('Theme', themeSchema)

class ResultsView {
    static render(theme, answers) {
        const { question } = theme

        return {
            question,
            answers
        }
    }
}

module.exports = ResultsView

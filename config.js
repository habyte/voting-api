const config = {
    server: {
        port: process.env.PORT || 3000
    },
    database: {
        address: process.env.DB_ADDRESS || 'mongodb://localhost:27017/voting',
        options: {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false,
        }
    }
}

module.exports = config
